-- Insert Teams
INSERT INTO team (name) VALUES
                            ('Development Team 1'),
                            ('Marketing Team 1'),
                            ('Support Team 1'),
                            ('Development Team 2'),
                            ('Marketing Team 2'),
                            ('Support Team 2'),
                            ('Sales Team 1'),
                            ('Sales Team 2'),
                            ('Research Team 1'),
                            ('Research Team 2');

-- Insert People and Set Team Leads
INSERT INTO employee (name, team_id) VALUES
                                         ('Person 1', 1), -- Development Team 1 Lead
                                         ('Person 2', 1),
                                         ('Person 3', 1),
                                         ('Person 4', 1),
                                         ('Person 5', 1),
                                         ('Person 6', 2), -- Marketing Team 1 Lead
                                         ('Person 7', 2),
                                         ('Person 8', 3), -- Support Team 1 Lead
                                         ('Person 9', 3),
                                         ('Person 10', 3),
                                         ('Person 11', 4), -- Development Team 2 Lead
                                         ('Person 12', 4),
                                         ('Person 13', 5), -- Marketing Team 2 Lead
                                         ('Person 14', 5),
                                         ('Person 15', 5),
                                         ('Person 16', 6), -- Support Team 2 Lead
                                         ('Person 17', 6),
                                         ('Person 18', 7), -- Sales Team 1 Lead
                                         ('Person 19', 7),
                                         ('Person 20', 7),
                                         ('Person 21', 8), -- Sales Team 2 Lead
                                         ('Person 22', 8),
                                         ('Person 23', 8),
                                         ('Person 24', 9), -- Research Team 1 Lead
                                         ('Person 25', 9),
                                         ('Person 26', 9),
                                         ('Person 27', 9),
                                         ('Person 28', 9),
                                         ('Person 29', 9),
                                         ('Person 30', 9),
                                         ('Person 31', 10), -- Research Team 2 Lead
                                         ('Person 32', 10),
                                         ('Person 33', 10),
                                         ('Person 34', 10),
                                         ('Person 35', 10),
                                         ('Person 36', 10);

-- Update Team Leads
UPDATE team SET team_lead_id = 1 WHERE id = 1; -- Person 1 is the Development Team 1 Lead
UPDATE team SET team_lead_id = 6 WHERE id = 2; -- Person 6 is the Marketing Team 1 Lead
UPDATE team SET team_lead_id = 8 WHERE id = 3; -- Person 8 is the Support Team 1 Lead
UPDATE team SET team_lead_id = 11 WHERE id = 4; -- Person 11 is the Development Team 2 Lead
UPDATE team SET team_lead_id = 13 WHERE id = 5; -- Person 13 is the Marketing Team 2 Lead
UPDATE team SET team_lead_id = 16 WHERE id = 6; -- Person 16 is the Support Team 2 Lead
UPDATE team SET team_lead_id = 18 WHERE id = 7; -- Person 18 is the Sales Team 1 Lead
UPDATE team SET team_lead_id = 21 WHERE id = 8; -- Person 21 is the Sales Team 2 Lead
UPDATE team SET team_lead_id = 24 WHERE id = 9; -- Person 24 is the Research Team 1 Lead
UPDATE team SET team_lead_id = 31 WHERE id = 10; -- Person 31 is the Research Team 2 Lead
