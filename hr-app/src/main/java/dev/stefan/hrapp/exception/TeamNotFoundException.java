package dev.stefan.hrapp.exception;

public class TeamNotFoundException extends RuntimeException {
    public TeamNotFoundException(String msg) {
        super(msg);
    }

}
