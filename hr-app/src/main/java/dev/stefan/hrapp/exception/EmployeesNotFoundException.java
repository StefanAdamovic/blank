package dev.stefan.hrapp.exception;

public class EmployeesNotFoundException extends RuntimeException {
    public EmployeesNotFoundException(String msg) {
        super(msg);
    }

}
