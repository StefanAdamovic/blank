package dev.stefan.hrapp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmployeeUpdateRequest {

    private long id;
    private String name;
    private long teamId;
}
