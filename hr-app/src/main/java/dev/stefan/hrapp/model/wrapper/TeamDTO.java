package dev.stefan.hrapp.model.wrapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TeamDTO {

    private long id;

    private String name;

    private EmployeeDTO teamLead;

    private long numberOfEmployees;
    @JsonIgnore
    private List<EmployeeDTO> employees;

}
