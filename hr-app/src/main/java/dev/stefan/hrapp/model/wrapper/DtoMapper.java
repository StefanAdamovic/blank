package dev.stefan.hrapp.model.wrapper;

import dev.stefan.hrapp.model.Employee;
import dev.stefan.hrapp.model.Team;

import java.util.List;

public class DtoMapper {

    private DtoMapper() {
    }

    public static EmployeeDTO mapEmployeeToEmployeeDTO(Employee employee) {
        EmployeeDTO employeeDTO = new EmployeeDTO();

        employeeDTO.setId(employee.getId());
        employeeDTO.setName(employee.getName());

        if (employee.getTeam() != null) {
            employeeDTO.setTeamName(employee.getTeam().getName());
            if (employee.getTeam().getTeamLead() != null)
                employeeDTO.setTeamLeadName(employee.getTeam().getTeamLead().getName());

        } else {
            employeeDTO.setTeamName("None");
            employeeDTO.setTeamLeadName("None");
        }
        return employeeDTO;
    }

    public static TeamDTO mapTeamToTeamDTO(Team team) {
        TeamDTO teamDTO = new TeamDTO();

        teamDTO.setId(team.getId());
        teamDTO.setName(team.getName());

        if (team.getTeamLead() != null)
            teamDTO.setTeamLead(DtoMapper.mapEmployeeToEmployeeDTO(team.getTeamLead()));

        if (team.getEmployees() != null) {
            List<EmployeeDTO> employeeDTOs = team.getEmployees().stream()
                    .map(DtoMapper::mapEmployeeToEmployeeDTO)
                    .toList();

            teamDTO.setEmployees(employeeDTOs);
            teamDTO.setNumberOfEmployees(employeeDTOs.size());
        }
        return teamDTO;
    }

}
