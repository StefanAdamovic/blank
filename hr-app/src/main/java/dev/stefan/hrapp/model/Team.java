package dev.stefan.hrapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @Column(unique = true)
    private String name;

    @JsonIgnore
    @OnDelete(action = OnDeleteAction.SET_NULL)
    @OneToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    @ToString.Exclude
    private Employee teamLead;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(mappedBy = "team", fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    // konsultacija kako na drugi nacin resiti lazy inizijalizaciju
    private List<Employee> employees;

}
