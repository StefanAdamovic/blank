package dev.stefan.hrapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;

    @OnDelete(action = OnDeleteAction.SET_NULL)
    @ManyToOne(cascade =
            {CascadeType.MERGE, CascadeType.REFRESH}
    )
    @JoinColumn(name = "team_id")
    @JsonIgnore
    private Team team;

    public Employee(String name, Team team) {
        this.name = name;
        this.team = team;
    }

}
