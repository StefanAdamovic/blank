package dev.stefan.hrapp.model.wrapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmployeeDTO {

    private long id;

    private String name;


    private String teamName;

    private String teamLeadName;

    public EmployeeDTO(String name, String teamName, String teamLeadName) {
        this.name = name;
        this.teamName = teamName;
        this.teamLeadName = teamLeadName;
    }

}
