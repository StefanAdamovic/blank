package dev.stefan.hrapp.service;

import dev.stefan.hrapp.model.Team;
import dev.stefan.hrapp.model.TeamCreateRequest;
import dev.stefan.hrapp.model.TeamUpdateRequest;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;
import dev.stefan.hrapp.model.wrapper.TeamDTO;

import java.util.List;

public interface TeamService {

    //Team findById(long id);
    TeamDTO findById(long id);

    List<TeamDTO> findAll();

    TeamDTO delete(long id);

    TeamDTO save(TeamCreateRequest teamCreateRequest);

    TeamDTO updateTeam(TeamUpdateRequest teamUpdateRequest);

    List<EmployeeDTO> getTeamEmployees(long id);

    EmployeeDTO removeEmployeeFromTeam(long teamId, long employeeId);

    EmployeeDTO getTeamEmployee(long teamId, long employeeId);

    List<TeamDTO> findBySizeAndType(long size, String teamType);
}
