package dev.stefan.hrapp.service;


import dev.stefan.hrapp.exception.EmployeeNotFoundException;
import dev.stefan.hrapp.exception.EmployeesNotFoundException;
import dev.stefan.hrapp.exception.TeamNotFoundException;
import dev.stefan.hrapp.model.Employee;
import dev.stefan.hrapp.model.EmployeeCreateRequest;
import dev.stefan.hrapp.model.EmployeeUpdateRequest;
import dev.stefan.hrapp.model.wrapper.DtoMapper;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;
import dev.stefan.hrapp.repository.EmployeeRepository;
import dev.stefan.hrapp.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final TeamRepository teamRepository;
    // TODO da li je bolje da komunicira sa team servisom ili team repository?


    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository, TeamRepository teamRepository) {
        this.employeeRepository = employeeRepository;
        this.teamRepository = teamRepository;

    }


    @Override
    public List<EmployeeDTO> findAll() {
        List<Employee> employees = employeeRepository.findAll();
        if (employees.isEmpty())
            throw new EmployeesNotFoundException("Employees not found");

        return employees.stream()
                .map(DtoMapper::mapEmployeeToEmployeeDTO)
                .collect(Collectors.toList());
    }


    @Override
    public List<Employee> findEmployeesByTeam(long teamId) {
        return employeeRepository.findEmployeesByTeam(teamId);
    }

    @Override
    public EmployeeDTO save(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = new Employee();
        employee.setName(employeeCreateRequest.getName());
        employee.setTeam(teamRepository
                .findById(employeeCreateRequest.getTeamId())
                .orElseThrow(() ->
                        new TeamNotFoundException("Team not found")));


        return DtoMapper.mapEmployeeToEmployeeDTO(employeeRepository.save(employee));

    }

    @Override
    public List<Employee> saveAll(List<Employee> employees) {
        return employeeRepository.saveAll(employees);
    }

    @Override
    public EmployeeDTO delete(long id) {

        EmployeeDTO employeeDTO = DtoMapper
                .mapEmployeeToEmployeeDTO(employeeRepository.findById(id)
                        .orElseThrow(() -> new EmployeeNotFoundException("Cant delete - Employee not found")));

        employeeRepository.deleteById(id);
        return employeeDTO;
    }


    @Override
    public EmployeeDTO findById(long id) {

        return DtoMapper
                .mapEmployeeToEmployeeDTO(employeeRepository.findById(id)
                        .orElseThrow(() ->
                                new EmployeeNotFoundException("Employee not found")));

    }

    @Override
    public EmployeeDTO updateEmployee(EmployeeUpdateRequest employeeUpdateRequest) {

        Employee employee = employeeRepository.findById(employeeUpdateRequest.getId())
                .orElseThrow(() ->
                        new EmployeeNotFoundException("Cant update - Employee not found"));

        Employee updatedEmployee = new Employee();

        updatedEmployee.setId(employee.getId());
        updatedEmployee.setName(employeeUpdateRequest.getName());
        updatedEmployee.setTeam(teamRepository
                .findById(employeeUpdateRequest.getTeamId())
                .orElseThrow(() ->
                        new TeamNotFoundException("Team not found")));

        return DtoMapper.mapEmployeeToEmployeeDTO(employeeRepository.save(updatedEmployee));
    }


}
