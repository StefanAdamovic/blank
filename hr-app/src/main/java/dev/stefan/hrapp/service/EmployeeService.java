package dev.stefan.hrapp.service;

import dev.stefan.hrapp.model.Employee;
import dev.stefan.hrapp.model.EmployeeCreateRequest;
import dev.stefan.hrapp.model.EmployeeUpdateRequest;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;

import java.util.List;

public interface EmployeeService {


    List<EmployeeDTO> findAll();

    List<Employee> findEmployeesByTeam(long teamId);

    EmployeeDTO save(EmployeeCreateRequest employeeCreateRequest);

    List<Employee> saveAll(List<Employee> employees);

    EmployeeDTO delete(long id);


    EmployeeDTO findById(long id);


    EmployeeDTO updateEmployee(EmployeeUpdateRequest employeeUpdateRequest);
}
