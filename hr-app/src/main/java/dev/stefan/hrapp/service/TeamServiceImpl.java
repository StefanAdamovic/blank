package dev.stefan.hrapp.service;

import dev.stefan.hrapp.exception.EmployeeNotFoundException;
import dev.stefan.hrapp.exception.TeamNotFoundException;
import dev.stefan.hrapp.model.Employee;
import dev.stefan.hrapp.model.Team;
import dev.stefan.hrapp.model.TeamCreateRequest;
import dev.stefan.hrapp.model.TeamUpdateRequest;
import dev.stefan.hrapp.model.wrapper.DtoMapper;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;
import dev.stefan.hrapp.model.wrapper.TeamDTO;
import dev.stefan.hrapp.repository.EmployeeRepository;
import dev.stefan.hrapp.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;
    private final EmployeeRepository employeeRepository;

    private final EmployeeService employeeService;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, EmployeeRepository employeeRepository, EmployeeService employeeService) {
        this.teamRepository = teamRepository;
        this.employeeRepository = employeeRepository;
        this.employeeService = employeeService;
    }


//    @Override
//    public Team findById(long id) {
//
//        return teamRepository.findById(id)
//                .orElseThrow(() ->
//                        new TeamNotFoundException("Team not found"));
//    }

    @Override
    public TeamDTO findById(long id) {

        return DtoMapper.mapTeamToTeamDTO(teamRepository.findById(id)
                .orElseThrow(() ->
                        new TeamNotFoundException("Team not found")));
    }

    @Override
    public List<TeamDTO> findAll() {
        List<Team> teams = teamRepository.findAll();
        if (teams.isEmpty())
            throw new TeamNotFoundException("Teams not found");

        return teams.stream()
                .map(DtoMapper::mapTeamToTeamDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TeamDTO delete(long id) {

        TeamDTO teamDTO = DtoMapper
                .mapTeamToTeamDTO(teamRepository.findById(id)
                        .orElseThrow(() -> new TeamNotFoundException("Cant delete - Team not found")));

        teamRepository.deleteById(id);
        return teamDTO;
    }

    @Override
    public TeamDTO save(TeamCreateRequest teamCreateRequest) {
        Team team = new Team();
        team.setName(teamCreateRequest.getName());

        return DtoMapper.mapTeamToTeamDTO(teamRepository.save(team));
    }

    @Override
    public TeamDTO updateTeam(TeamUpdateRequest teamUpdateRequest) {

        Team team = teamRepository.findById(teamUpdateRequest.getId())
                .orElseThrow(() ->
                        new TeamNotFoundException("Cant update - Team not found"));

        team.setName(teamUpdateRequest.getName());

        return DtoMapper.mapTeamToTeamDTO(teamRepository.save(team));
    }

    @Override
    public List<EmployeeDTO> getTeamEmployees(long id) {

        TeamDTO teamDTO = findById(id);

        return teamDTO.getEmployees();


    }

    @Override
    public EmployeeDTO removeEmployeeFromTeam(long teamId, long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() ->
                        new EmployeeNotFoundException("Cant remove - Employee not found"));

        if (employee.getTeam() == null)
            throw new TeamNotFoundException("Employee already doesnt have team");
        if (employee.getTeam().getId() != teamId)
            throw new TeamNotFoundException("Wrong team or employee");
// TODO promeniti exception
        employee.setTeam(null);
        return DtoMapper.mapEmployeeToEmployeeDTO(employeeRepository.save(employee));
    }

    @Override
    public EmployeeDTO getTeamEmployee(long teamId, long employeeId) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() ->
                        new EmployeeNotFoundException("Employee not found"));

        Team team = teamRepository.findById(teamId)
                .orElseThrow(() ->
                        new TeamNotFoundException("Team not found"));

        if (employee.getTeam().getId() != team.getId()) {
            throw new EmployeeNotFoundException("Employee not part of team with id: " + teamId);
        }
        return DtoMapper.mapEmployeeToEmployeeDTO(employee);

    }

    @Override
    public List<TeamDTO> findBySizeAndType(long size, String teamType) {

        List<Team> teams = teamRepository.findAll();
        List<Team> teamsFilteredByType = new ArrayList<>();
        List<Team> teamsFilteredBySize = new ArrayList<>();

        if (size == 0 && teamType.equalsIgnoreCase("all")) {
            return teams.stream()
                    .map(DtoMapper::mapTeamToTeamDTO)
                    .collect(Collectors.toList());
        }

        if (size == 0) {
            teamsFilteredByType = teams.stream().filter((t) -> t.getName().toLowerCase().contains(teamType)).toList();

            return teamsFilteredByType.stream()
                    .map(DtoMapper::mapTeamToTeamDTO)
                    .collect(Collectors.toList());
        }

        if (teamType.equalsIgnoreCase("all")) {

            teamsFilteredBySize = teams.stream().filter((t) -> t.getEmployees().size() >= size).toList();
            return teamsFilteredBySize.stream()
                    .map(DtoMapper::mapTeamToTeamDTO)
                    .collect(Collectors.toList());

        }

        teamsFilteredByType = teams.stream().filter((t) -> t.getName().toLowerCase().contains(teamType)).toList();
        if (teamsFilteredByType.isEmpty())
            throw new TeamNotFoundException("Team By Type Not Found");

        teamsFilteredBySize = teamsFilteredByType.stream().filter((t) -> t.getEmployees().size() >= size).toList();

        if (teamsFilteredBySize.isEmpty())
            throw new TeamNotFoundException("Team By Size Not Found");


        return teamsFilteredBySize.stream()
                .map(DtoMapper::mapTeamToTeamDTO)
                .collect(Collectors.toList());

    }
}
