package dev.stefan.hrapp.controller;

import dev.stefan.hrapp.model.EmployeeCreateRequest;
import dev.stefan.hrapp.model.EmployeeUpdateRequest;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;
import dev.stefan.hrapp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public ResponseEntity<List<EmployeeDTO>> getEmployees() {

        return ResponseEntity.ok(employeeService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeDTO> getEmployeeById(@PathVariable long id) {

        return ResponseEntity.ok(employeeService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<EmployeeDTO> deleteEmployee(@PathVariable long id) {

        return ResponseEntity.ok(employeeService.delete(id));
    }

    @PostMapping
    public ResponseEntity<EmployeeDTO> createEmployee(@RequestBody EmployeeCreateRequest employeeCreateRequest) {

        return ResponseEntity.ok(employeeService.save(employeeCreateRequest));

    }


    @PutMapping("/{id}")
    public ResponseEntity<EmployeeDTO> updateEmployee(
            @PathVariable long id,
            @RequestBody EmployeeUpdateRequest employeeUpdateRequest) {

        employeeUpdateRequest.setId(id);
        return ResponseEntity.ok(employeeService.updateEmployee(employeeUpdateRequest));
    }

}
