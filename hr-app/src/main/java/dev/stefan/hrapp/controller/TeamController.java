package dev.stefan.hrapp.controller;


import dev.stefan.hrapp.model.TeamCreateRequest;
import dev.stefan.hrapp.model.TeamUpdateRequest;
import dev.stefan.hrapp.model.wrapper.EmployeeDTO;
import dev.stefan.hrapp.model.wrapper.TeamDTO;
import dev.stefan.hrapp.service.TeamService;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("teams")
public class TeamController {

    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping("/filter")
    public ResponseEntity<List<TeamDTO>> getTeams
            (@RequestParam(defaultValue = "0") long size,
             @RequestParam(name = "teamtype", defaultValue = "all") String teamType) {

        return ResponseEntity.ok(teamService.findBySizeAndType(size, teamType.toLowerCase()));
    }


    @GetMapping
    public ResponseEntity<List<TeamDTO>> getTeams() {

        return ResponseEntity.ok(teamService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TeamDTO> getTeamById(@PathVariable long id) {

        return ResponseEntity.ok(teamService.findById(id));
    }

    @GetMapping("/{id}/employees")
    public ResponseEntity<List<EmployeeDTO>> getTeamEmployees(@PathVariable long id) {

        return ResponseEntity.ok(teamService.getTeamEmployees(id));
    }

    @GetMapping("/{teamId}/employees/{employeeId}")
    public ResponseEntity<EmployeeDTO> getTeamEmployee(
            @PathVariable long teamId, @PathVariable long employeeId) {

        return ResponseEntity.ok(teamService.getTeamEmployee(teamId, employeeId));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<TeamDTO> deleteTeam(@PathVariable long id) {

        return ResponseEntity.ok(teamService.delete(id));
    }

    @DeleteMapping("/{teamId}/employees/{employeeId}")
    public ResponseEntity<EmployeeDTO> deleteEmployeeFromTeam(
            @PathVariable long teamId, @PathVariable long employeeId) {

        return ResponseEntity.ok(teamService.removeEmployeeFromTeam(teamId, employeeId));
    }

    @PostMapping
    public ResponseEntity<TeamDTO> createTeam(@RequestBody TeamCreateRequest teamCreateRequest) {

        return ResponseEntity.ok(teamService.save(teamCreateRequest));

    }

    @PutMapping("/{id}")
    public ResponseEntity<TeamDTO> updateTeam(
            @PathVariable long id,
            @RequestBody TeamUpdateRequest teamUpdateRequest) {

        teamUpdateRequest.setId(id);
        return ResponseEntity.ok(teamService.updateTeam(teamUpdateRequest));
    }


    //TODO zavrsiti
    @PutMapping("/{teamId}/{employeeId}")
    public ResponseEntity<TeamDTO> updateTeamLeadOfTeam(
            @PathVariable long teamId, @PathVariable long employeeId) {
        throw new NotImplementedException("Metoda nije implementirana");
        //return ResponseEntity.ok(teamService.updateTeamLeadOfTeam(teamId, employeeId));
    }


    @PutMapping("/{teamId}/employees/{employeeId}/{newTeamId}")
    public ResponseEntity<TeamDTO> transferEmployeeToAnotherTeam(
            @PathVariable long teamId, @PathVariable long employeeId, @PathVariable long newTeamId) {
        throw new NotImplementedException("Metoda nije implementirana");
        //return ResponseEntity.ok(teamService.transferEmployeeToTeam(teamId, employeeId, newTeamId));
    }


}
