package dev.stefan.hrapp.repository;

import dev.stefan.hrapp.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("SELECT e FROM Employee e WHERE e.team.id = :teamId order by e.team.id")
    List<Employee> findEmployeesByTeam(long teamId);
}
